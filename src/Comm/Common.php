<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2018/7/20
 * Time: 10:43
 */

namespace BaiDuStatistics\Comm;

class Common
{
    // 参数配置
    public static $config = [
        "loginUrl"    => "",
        "apiUrl"      => "",
        "username"    => "",
        "password"    => "",
        "token"       => "",
        "uuid"        => "",
        "accountType" => 1,
        "puk"         => "",
    ];


    // 对应的域名,站点id (一次查询出来就可)
    public static $siteId = [];

    /**
     * @param array $config
     * @throws \Exception
     */
    public static function setConfig(array $config = [])
    {
        self::$config = array_merge(self::$config, $config);

        if (in_array("", self::$config)) {
            throw new \Exception('配置参数有误');
        }
    }

    /**
     * @param string $name  对应域名
     * @param int $value   对应的站点id
     */
    public static function addSiteId(string $name, int $value)
    {
        self::$siteId[$name] = $value;
    }

}