<?php
/**
 * class ReportService
 */
namespace BaiDuStatistics\Others;

/**
 * ReportService
 */
class ReportService
{

    private $apiUrl;
    private $userName;
    private $token;
    private $ucid;
    private $st;
    private $accpuntType;
    
    /**
     * construct
     * @param string $apiUrl
     * @param string $userName
     * @param string $token
     * @param string $ucid
     * @param string $st
     */
    public function __construct($apiUrl, $userName, $token, $accountType, $ucid, $st) {
        $this->apiUrl = $apiUrl;
        $this->userName = $userName;
        $this->token = $token;
        $this->accpuntType = $accountType;
        $this->ucid = $ucid;
        $this->st = $st;
    }
    
    /**
     * get site list
     * @return array
     */
    public function getSiteList() {
        echo '----------------------get site list----------------------' . PHP_EOL;
        $apiConnection = new DataApiConnection();
        $apiConnection->init($this->apiUrl . '/getSiteList', $this->ucid);

        $apiConnectionData = array(
            'header' => array(
                'username' => $this->userName,
                'password' => $this->st,
                'token' => $this->token,
                'account_type' =>  $this->accpuntType,
            ),
            'body' => null,
        );
        $apiConnection->POST($apiConnectionData);
        
        return array(
            'header' => $apiConnection->retHead,
            'body' => $apiConnection->retBody,
            'raw' => $apiConnection->retRaw,
        );
    }

    /**
     * get data
     * @param array $parameters
     * @return array
     */
    public function getData($parameters) {
        echo '----------------------get data----------------------' . PHP_EOL;
        $apiConnection = new DataApiConnection();
        $apiConnection->init($this->apiUrl . '/getData', $this->ucid);

        $apiConnectionData = array(
            'header' => array(
                'username' => $this->userName,
                'password' => $this->st,
                'token' => $this->token,
                'account_type' =>  $this->accpuntType,
            ),
            'body' => $parameters,
        );
        $apiConnection->POST($apiConnectionData);
        
        return array(
            'header' => $apiConnection->retHead,
            'body'   => $apiConnection->retBody,
            'raw'    => $apiConnection->retRaw,
        );
    }
}
