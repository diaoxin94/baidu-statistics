<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 2018/7/20
 * Time: 9:28
 */

namespace BaiDuStatistics;

use BaiDuStatistics\Comm\Common;
use BaiDuStatistics\Login\LoginService;
use BaiDuStatistics\Others\ReportService;

// 获取统计信息
class GetTongJiData
{
    protected $uCid;
    protected $st;
    protected $ret;


    public function __construct(array $config = [])
    {
        Common::setConfig($config);
    }

    /**
     * 登录 验证
     * @return bool
     */
    public function login()
    {
        $loginService = new LoginService(Common::$config["loginUrl"], Common::$config["uuid"]);
        if (!$loginService->preLogin(Common::$config["username"], Common::$config["token"])) {

            return false;
        }

        $res = $loginService->doLogin(Common::$config["username"], Common::$config["password"], Common::$config["token"]);
        if (!$res) {

            return false;
        }

        $this->uCid = $res['ucid'];
        $this->st   = $res['st'];

        return true;
    }

    /**
     * 获取站点列表
     * @return array
     */
    public function getList()
    {
        $reportService = new ReportService(Common::$config["apiUrl"],
            Common::$config["username"],
            Common::$config["token"],
            Common::$config["accountType"],
            $this->uCid,
            $this->st);

        $this->ret = $reportService->getSiteList();

        return $this->ret;
    }

    /**
     * 获取站点数据
     * @param int $siteId
     * @param int $startDate
     * @param int $endDate
     * @param string|null $gran
     * @param string|null $method
     * @param string|null $metrics
     * @return array
     */
    public function getTongJiData(
        int $siteId,
        int $startDate,
        int $endDate,
        string $gran = null,
        string $method = null,
        string $metrics = null)
    {

        $reportService = new ReportService(Common::$config["apiUrl"],
            Common::$config["username"],
            Common::$config["token"],
            Common::$config["accountType"],
            $this->uCid,
            $this->st);

        $array = [
            'site_id'    => $siteId,                            //站点ID
            'method'     => $method ?? "trend/time/a",          //趋势分析报告
            'start_date' => $startDate,                         //所查询数据的起始日期  20160531
            'end_date'   => $endDate,                           //所查询数据的结束日期  20160531
            'max_results'=> 0,                                  //返回所有条数
            'gran'       => $gran ?? "hour",                    //按天粒度
            'metrics'    => $metrics ?? "pv_count,visitor_count", //所查询指标为PV和UV
        ];

        if ($method == 'source/engine/a') {
            unset($array['gran']);
        }

        $ret = $reportService->getData($array);

        return $ret;
    }


    /**
     * 退出登录
     * @return bool
     */
    public function loginOut()
    {
        $loginService = new LoginService(Common::$config["loginUrl"], Common::$config["uuid"]);

        return $loginService->doLogout(Common::$config["username"], Common::$config["token"], $this->uCid, $this->st);
    }
}