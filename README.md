# baidu-statistics

#### 介绍

百度网站统计的 api 接口，具体情况可查看官方接口文档

https://tongji.baidu.com/api/manual/

#### 软件架构

PHP >= 7.0

#### 安装教程

composer require diaoxin/baidu-statistics

composer.json 源请更换为阿里云，速度快一点
```json
"repositories": {
    "packagist": {
        "type": "composer",
        "url": "https://mirrors.aliyun.com/composer/"
    }
}
```

#### 使用说明

```php
$config = [
    "loginUrl" => 'https://api.baidu.com/sem/common/HolmesLoginService', // 登录地址
    "apiUrl"   => 'https://api.baidu.com/json/tongji/v1/ReportService', //  接口地址
    "username" => '用户名',  
    "password" => '密码',
    "token"    => 'token 值',
    "uuid"     => 'uuid',
    "accountType" => 1,
    "puk"      => "-----BEGIN PUBLIC KEY-----
                   MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDHn/hfvTLRXViBXTmBhNYEIJeG
                   GGDkmrYBxCRelriLEYEcrwWrzp0au9nEISpjMlXeEW4+T82bCM22+JUXZpIga5qd
                   BrPkjU08Ktf5n7Nsd7n9ZeI0YoAKCub3ulVExcxGeS3RVxFai9ozERlavpoTOdUz
                   EH6YWHP4reFfpMpLzwIDAQAB
                   -----END PUBLIC KEY-----", // openssl 公钥
];

// 实例化获取数据类
$reslut = new GetTongJiData($config);
//登录
//$login = $reslut->login();
// 获取站点列表
//$reslut->getList();
// 添加站点信息
//Common::addSiteId("域名", "站点id");
// 获取站点数据
//$data = $reslut->getTongJiData(Common::$siteId["域名"],20190501,20190530);
// 退出
//$reslut->loginOut();
```